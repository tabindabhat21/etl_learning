import argparse
from ast import arguments
import datetime
import datetime as dt
import pandas as pd
import numpy as np
import hashlib


def main():
    # inititize the parser
    parser = argparse.ArgumentParser(description="my etl script")

    # add parameters which you want to parse

    parser.add_argument('-P', '--Path',
                        help="Path to the file", type=str)
    parser.add_argument('-L', '--LocalSubjectID',
                        help="Local subject Id to add to the file", type=str)
    parser.add_argument('-G', '--GlobalSubjectID',
                        help="Global subject Id to add to the file", type=str)
    parser.add_argument('-S', '--Sourcename',
                        help="Source name to add to the file", type=str)
    parser.add_argument(
        '-s', '--source', help="source to add to the file", type=str)
    arguments = parser.parse_args()
    return arguments


def add_columns(path, local_subject_id,global_subject_id,source_name,source):
    df = pd.read_excel(path, engine="openpyxl")
    # Adding SourceRowIndex as an index column
    df['SourceRowIndex'] = np.arange(len(df))
    df.set_index("SourceRowIndex", inplace=True)

    # Splitting duration column into hours, minutes and seconds to add it to the UTC - START column
    hours = list(df['DURATION'].apply(lambda x: pd.to_datetime(
        str(x), utc=True).replace(tzinfo=None).strftime('%H.%f')))
    minutes = list(df['DURATION'].apply(lambda x: pd.to_datetime(
        str(x), utc=True).replace(tzinfo=None).strftime('%M.%f')))
    seconds = list(df['DURATION'].apply(lambda x: pd.to_datetime(
        str(x), utc=True).replace(tzinfo=None).strftime('%S.%f')))

    # Put UTC - START column into a list and then add hours, minutes and seconds to it using timedelta
    start = list(df["UTC - START"])
    end = [(start[i] + datetime.timedelta(hours=int(float(hours[i])), minutes=int(
        float(minutes[i])), seconds=int(float(seconds[i])))) for i in range(len(minutes))]

    # To change the UTC - START and GH_$_END to the format specified
    df["StartDate"] = df["UTC - START"].apply(lambda x: pd.to_datetime(
        x, utc=True).replace(tzinfo=None).strftime('%Y-%m-%d %H:%M:%S.%f'))
    df["EndDate"] = end
    df["EndDate"] = df["EndDate"].apply(lambda x: pd.to_datetime(
        x, utc=True).replace(tzinfo=None).strftime('%Y-%m-%d %H:%M:%S.%f'))

    df["UTC - START"] = df["UTC - START"].apply(
        lambda x: x.strftime('%m/%d/%Y %H:%M:%S'))
    df = df.rename(columns={"EVENT": "GH_$_EVENT",
                            "DURATION": "GH_$_DURATION", "UTC - START": "GH_$_UTC - START"})

    # Adding other columns
    df["SourceName"] = source_name
    df["SourceMeta"] = "{}"
    df["source"] = source
    df["LocalSubjectID"] = local_subject_id
    df["GlobalSubjectID"] = global_subject_id
    df["Email"] = "Inspiration-4"
    df["variablename"] = "timelineData"

    # Adding secure hashes
    df["source_id"] = (df["GH_$_EVENT"]
                       ).apply(lambda x: hashlib.sha256(str.encode(x)).hexdigest())

    df.to_csv("final.csv")
    return df


if __name__ == '__main__':

    arguments = main()
    a = add_columns(arguments.Path, arguments.LocalSubjectID, arguments.GlobalSubjectID,
                    arguments.Sourcename, arguments.source)
    print(a.columns)